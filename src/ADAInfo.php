<?php

namespace Drupal\ada_module;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Http\ClientFactory;

/**
 * Class for managing ADAInfo.
 */
class ADAInfo {
  /**
   * Http client to make requests.
   *
   * @var GuzzleHttp\ClientInterface
   */
  private $client;

  /**
   * ADA installation base URL.
   *
   * @var strring
   */
  private $baseUrl;

  /**
   * ADAInfo constructor.
   *
   * @param Drupal\Core\Http\ClientFactory $factory
   *   The injected ClientFactory.
   * @param Drupal\Core\Config\ConfigFactory $configFactory
   *   The injected ConfigFactory.
   */
  public function __construct(ClientFactory $factory, ConfigFactory $configFactory) {
    $this->client = $factory->fromOptions();
    $this->setBaseUrl($configFactory->get('ada.application_settings')->get('ada_url'));
  }

  /**
   * Loads the list of available courses.
   *
   * @return array
   *   The array of loaded courses.
   */
  public function getCourses() {
    if (!empty($this->getBaseUrl())) {
      $req = $this->client->get($this->getBaseUrl() . '/api/v1/info');
      return Json::decode($req->getBody()->getContents());
    }
    return [];
  }

  /**
   * BaseUrl setter.
   *
   * @param string $baseUrl
   *   The ADA url.
   *
   * @return self
   *   self object
   */
  public function setBaseUrl($baseUrl = NULL) {
    $this->baseUrl = $baseUrl;

    return $this;
  }

  /**
   * BaseUrl getter.
   *
   * @return string
   *   The ADA base url.
   */
  public function getBaseUrl() {
    return $this->baseUrl;
  }

}
