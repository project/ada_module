<?php

namespace Drupal\ada_module\Form;

use Drupal\ada_module\ADAInfo;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ApplicationSettings to manage the ADA Module settings.
 */
class ApplicationSettings extends ConfigFormBase {

  /**
   * Client to make requests to the ADA API.
   *
   * @var Drupal\ada_module\ADAInfo
   */
  protected $adaClient;

  /**
   * Constructs a \Drupal\ada_module\Form\ApplicationSettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param Drupal\ada_module\ADAInfo $adaClient
   *   The injected AdaClient.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ADAInfo $adaClient) {
    $this->adaClient = $adaClient;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('ada.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ada_application_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ada.application_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ada.application_settings');

    $instructions = "<p>" .
      $this->t("This is the main ADA Drupal module. In order to communicate with ADA please configure it using this form") .
      "</p>";

    $form['instructions'] = [
      '#markup' => $instructions,
    ];

    $form['ada_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Main URL of your ADA installation'),
      '#description' => $this->t('Enter the url of your ADA installation. E.g.: <a href="https://learning.lynxlab.com" target="_blank">https://learning.lynxlab.com</a>.'),
      '#default_value' => $config->get('ada_url'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('ada_url', rtrim($form_state->getValue('ada_url'), '/'));
    $this->adaClient->setBaseUrl($form_state->getValue('ada_url'));
    try {
      $this->adaClient->getCourses();
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('ada_url',
        $this->t("Could not resolve hostname @url, or is not an ADA installation", [
          '@url' => $form_state->getValue('ada_url'),
        ])
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ada.application_settings')
      ->set('ada_url', $form_state->getValue('ada_url'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
