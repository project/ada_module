<?php

namespace Drupal\ada_module\Plugin\Block;

use Drupal\ada_module\ADAInfo;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Block of Ada Courses.
 *
 * @Block(
 *   id = "ada-courses-block",
 *   admin_label = @Translation("ADA Courses")
 * )
 */
class AdaCourses extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Client to make requests to the ADA API.
   *
   * @var Drupal\ada_module\ADAInfo
   */
  protected $adaClient;

  /**
   * AdaCourses constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param Drupal\ada_module\ADAInfo $adaClient
   *   The injected AdaClient.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ADAInfo $adaClient) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->adaClient = $adaClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ada.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $adaCourses = $this->adaClient->getCourses();
    $items = [];

    foreach ($adaCourses as $course) {
      if (array_key_exists('link', $course)) {
        $items[] = [
          '#title' => $course['name'],
          '#type' => 'link',
          '#attributes' => ['title' => $course['name'], 'target' => '_blank'],
          '#url' => Url::fromUri($course['link']),
        ];
      }
      else {
        $items[] = $course['name'];
      }
    }

    return [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTags() {
    return ['ada-courses-block'];
  }

}
