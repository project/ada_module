<?php

/**
 * @file
 * This module holds functions useful for Drupal development.
 *
 * Please contribute!
 */

/**
 * Implements hook_views_data().
 */
function ada_views_views_data() {
  $data = [];
  // Base data.
  $data['ada_course']['table']['group'] = t('ADA Courses');
  $data['ada_course']['table']['base'] = [
    'title' => t('ADA Courses'),
    'help' => t("ADA courses data provided by the ADA API's Info endpoint."),
    'query_id' => 'ada-courses',
  ];

  // Fields.
  $data['ada_course']['name'] = [
    'title' => t('Course Title'),
    'help' => t('ADA Course Title'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'ada_course_name',
    ],
    'sort' => [
      'id' => 'ada_course_name',
    ],
  ];
  $data['ada_course']['description'] = [
    'title' => t('Course Description'),
    'help' => t('ADA Course Description, will contain HTML'),
    'field' => [
      'id' => 'ada_info_course_description',
    ],
  ];
  $data['ada_course']['provider_pointer'] = [
    'title' => t('Course Provider Pointer'),
    'help' => t('ADA Course Provider Pointer, like a unique id for the provider of the course'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'ada_course_provider_pointer',
    ],
  ];
  $data['ada_course']['provider_name'] = [
    'title' => t('Course Provider Name'),
    'help' => t('ADA Course Provider Name, name of the provider of the course'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'ada_course_provider_name',
    ],
    'sort' => [
      'id' => 'ada_course_provider_name',
    ],
  ];
  $data['ada_course']['subscription_link'] = [
    'title' => t('Course Subscription link'),
    'help' => t('Link to subscribe to an ADA Course'),
    'field' => [
      'id' => 'ada_info_subscribe_link',
    ],
  ];

  return $data;
}
