<?php

namespace Drupal\ada_views\Plugin\views\query;

use Drupal\ada_module\ADAInfo;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ADA views query, wraps calls to the ADA API to expose results to views.
 *
 * @ViewsQuery(
 *   id = "ada-courses",
 *   title = @Translation("ADA Courses Query"),
 *   help = @Translation("Query against the ADA API info endpoint.")
 * )
 */
class AdaCoursesQuery extends QueryPluginBase {

  /**
   * Client to make requests to the ADA API.
   *
   * @var Drupal\ada_module\ADAInfo
   */
  protected $adaClient;

  /**
   * AdaCoursesQuery constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param Drupal\ada_module\ADAInfo $adaClient
   *   The injected AdaClient.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ADAInfo $adaClient) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->adaClient = $adaClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ada.info'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function build(ViewExecutable $view) {
    $view->initPager();
    /*
     * Should pager disappear, try to uncomment this line:
     * $view->display_handler->options['cache']['type'] = 'none';
     */
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ViewExecutable $view) {
    $start = microtime(TRUE);
    $filters = [];
    $rows = [];
    $orderby = [];

    if (isset($this->where)) {
      foreach ($this->where as $where) {
        foreach ($where['conditions'] as $condition) {
          // Ensure value is always an array.
          if (!is_array($condition['value'])) {
            $condition['value'] = [$condition['value']];
          }
          // Add filter only if some value is there.
          if (strlen(implode('', $condition['value'])) > 0) {
            // Remove dot from beginning of the string.
            $field_name = ltrim($condition['field'], '.');
            $filters[$field_name] = [
              'value' => $condition['value'],
              'operator' => $condition['operator'],
            ];
          }
        }
      }
    }

    if (isset($this->orderby)) {
      /*
       * Get the first orderby only.
       *
       * @todo implement full multiple array orderby
       */
      $orderby = reset($this->orderby);
    }

    $resultsArr = $this->adaClient->getCourses();
    if (is_array($resultsArr) && count($resultsArr) > 0) {
      foreach ($resultsArr as $index => $res) {
        $row = [];
        $row['name'] = $res['name'];
        $row['description'] = $res['description'];
        $row['provider_pointer'] = $res['provider']['pointer'];
        $row['provider_name'] = $res['provider']['name'];
        if (array_key_exists('link', $res)) {
          $row['subscription_link'] = $res['link'];
        }
        else {
          $row['subscription_link'] = NULL;
        }
        array_push($rows, $row);
      }
    }

    // Apply filters.
    if (count($filters) > 0) {
      foreach ($filters as $key => $filter) {
        $rows = array_filter($rows, function ($row) use ($key, $filter) {
          if (isset($row[$key])) {
            // Support only '=' operator.
            if ($filter['operator'] === '=') {
              if (is_string($row[$key])) {
                foreach ($filter['value'] as $searchStr) {
                  if (\mb_stristr($row[$key], $searchStr) !== FALSE) {
                    return TRUE;
                  }
                }
              }
              else {
                return in_array($row[$key], $filter['value'], TRUE);
              }
            }
          }
          return FALSE;
        });
      }
    }

    // Sort results.
    if (count($orderby) > 0) {
      usort($rows, function ($a, $b) use ($orderby) {
        if ($a[$orderby['field']] == $b[$orderby['field']]) {
          return 0;
        }
        elseif (is_string($a[$orderby['field']])) {
          $check = strcasecmp($a[$orderby['field']], $b[$orderby['field']]);
        }
        elseif (is_numeric($a[$orderby['field']])) {
          $check = $a[$orderby['field']] < $b[$orderby['field']] ? -1 : 1;
        }
        return ('ASC' === $orderby['direction'] ? $check : -1 * $check);
      });
    }

    $view->total_rows = count($rows);
    $view->pager->total_items = $view->total_rows;
    $view->pager->updatePageInfo();

    if ($view->usePager()) {
      if (array_key_exists('items_per_page', $view->getExposedInput())) {
        if (is_numeric($view->getExposedInput()['items_per_page'])) {
          $perPage = (int) $view->getExposedInput()['items_per_page'];
        }
        else {
          $perPage = 0;
        }
        $view->pager->setItemsPerPage($perPage);
      }
      else {
        $view->setCurrentPage($view->pager->getCurrentPage());
      }
      $view->setItemsPerPage($view->pager->getItemsPerPage());

      if ($view->getItemsPerPage() > 0) {
        $view->pager->total_items = count($rows);
        $rows = array_splice($rows, (int) $view->getItemsPerPage() * (int) $view->getCurrentPage(), $view->getItemsPerPage());
        $view->pager->updatePageInfo();
        $view->pager->setCurrentPage($view->getCurrentPage());
      }
    }

    if (count($rows) > 0) {
      foreach ($rows as $index => $row) {
        // 'index' key is required.
        $row['index'] = $index++;
        $view->result[] = new ResultRow($row);
      }
    }
    $view->execute_time = microtime(TRUE) - $start;
  }

  /**
   * {@inheritDoc}
   */
  public function addWhere($group, $field, $value = NULL, $operator = NULL) {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }
    // Check for a group.
    if (!isset($this->where[$group])) {
      $this->setWhereGroup('AND', $group);
    }
    $this->where[$group]['conditions'][] = [
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function addOrderBy($table, $field = NULL, $order = 'ASC', $alias = '', $params = []) {

    // Only ensure the table if it's not the special random key.
    // @todo Maybe it would make sense to just add an addOrderByRand or something similar.
    if ($table && $table != 'rand') {
      $this
        ->ensureTable($table);
    }

    // Only fill out this aliasing if there is a table;
    // otherwise we assume it is a formula.
    if (!$alias && $table) {
      $as = $table . '_' . $field;
    }
    else {
      $as = $alias;
    }
    if ($field) {
      $as = $this
        ->addField($table, $field, $as, $params);
    }
    $this->orderby[] = [
      'field' => $as,
      'direction' => strtoupper($order),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTags() {
    return ['ada-courses-query'];
  }

  /**
   * Workaround for the Views core that assume an SQL-query backend.
   *
   * Method ensureTable is used by Views to make sure that the generated SQL
   * query contains the appropriate JOINs to ensure that a given table is
   * included in the results.
   *
   * {@inheritDoc}
   */
  public function ensureTable($table, $relationship = NULL) {
    return '';
  }

  /**
   * Workaround for the Views core that assume an SQL-query backend.
   *
   * Method addField is used by Views core to limit the fields that are part of
   * the result set. In our case, the ADA API has no way to limit the fields
   * that come back in an API response, so we don’t need this
   * We'll always provide values from the result set, which we defined
   * in hook_views_data.
   *
   * {@inheritDoc}
   */
  public function addField($table, $field, $alias = '', $params = []) {
    return $field;
  }

}
