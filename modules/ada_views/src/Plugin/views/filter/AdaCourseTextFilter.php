<?php

namespace Drupal\ada_views\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Abastract class AdaCourseTextFilter.
 */
abstract class AdaCourseTextFilter extends FilterPluginBase {

  /**
   * Disable the possibility to use operators.
   *
   * @var bool
   */
  public $no_operator = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#size' => 30,
      '#default_value' => $this->value,
    ];
  }

}
