<?php

namespace Drupal\ada_views\Plugin\views\filter;

/**
 * Filter to handle filtering ADA courses by provider.
 *
 * @ViewsFilter("ada_course_name")
 */
class AdaCourseName extends AdaCourseTextFilter {
}
