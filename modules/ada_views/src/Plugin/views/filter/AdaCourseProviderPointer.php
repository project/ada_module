<?php

namespace Drupal\ada_views\Plugin\views\filter;

/**
 * Filter to handle filtering ADA courses by provider.
 *
 * @ViewsFilter("ada_course_provider_pointer")
 */
class AdaCourseProviderPointer extends AdaCourseTextFilter {
}
