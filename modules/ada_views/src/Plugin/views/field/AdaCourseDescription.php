<?php

namespace Drupal\ada_views\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Class AdaCourseDescription for managing the description field.
 *
 * @ViewsField("ada_info_course_description")
 */
class AdaCourseDescription extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    return [
      '#markup' => $this->getValue($values),
    ];
  }

}
