<?php

namespace Drupal\ada_views\Plugin\views\field;

use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Class AdaSubscribeLink for managing the subscribe link field.
 *
 * @ViewsField("ada_info_subscribe_link")
 */
class AdaSubscribeLink extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $link = $this->getValue($values);

    if ($link) {
      if (property_exists($values, 'name')) {
        $title = $values->name;
      }
      else {
        $title = NULL;
      }
      return [
        '#title' => $this->t('Subscribe'),
        '#type' => 'link',
        '#attributes' => [
          'class' => 'btn',
          'title' => $title,
          'target' => '_blank',
        ],
        '#url' => Url::fromUri($link),
      ];
    }
  }

}
