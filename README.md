# Drupal Module for Interacting with Lynx ADA LMS

This module will integrate into Drupal the courses list provided by the ADA LMS Rest API.

## Install

Use composer:

```composer require 'drupal/ada_module'```

Download module compressed archive from:

<https://www.drupal.org/project/ada_module/releases>

## Use

1. After installation, enable _ADA Module_ and _ADA Views_ from drupal admin/modules page or using drush

2. Go to admin/config page and click the _ADA Application Settings_ link in the _Web Service_ section

3. Fill in the ADA Settings form with the URL of your ADA Installation (e.g. <https://learning.lynxlab.com>)

4. You now have available the _ADA Courses_ block and _ADA Courses_ view and may use it as usual.

## Clone source code

```git clone https://git.drupalcode.org/project/ada_module.git```

## Known Problems

- Exposed fields such as filters and pager options will not work when Use AJAX is set to Yes.

## About ADA LMS

More info about ADA Learning Management System here: [Lynx](https://www.lynxlab.com).
